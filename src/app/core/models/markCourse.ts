export class MarkCourse {
  _id?: string;
  markDesc: string;
  courseNumber: string;
  courseName: string;
  studentsId?: string;
  markPoint?: string;
  grade?: number;
  examStatus?: boolean;
}
