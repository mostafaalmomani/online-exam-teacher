import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  theme: string;
  options = {
    title: {
      text: 'Nightingale\'s Rose Diagram',
      subtext: 'Mocking Data',
      x: 'center'
    },
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
      x: 'center',
      y: 'bottom',
      data: ['الطلاب الناجحين', 'طلاب تحت المعدل', 'rose3', 'rose4', 'rose5', 'rose6', 'rose7', 'rose8']
    },
    calculable: true,
    series: [
      {
        name: 'area',
        type: 'pie',
        radius: [30, 110],
        roseType: 'area',
        data: [
          { value: 10, name: 'الطلاب الناجحين' },
          { value: 5, name: 'طلاب تحت المعدل' },
          { value: 15, name: 'الطلاب الراسبين' },
          { value: 25, name: 'طلاب  تحت المعدل' },
          { value: 20, name: 'المعدل' },
          // { value: 35, name: 'rose6' },
          // { value: 30, name: 'rose7' },
          // { value: 40, name: 'rose8' }
        ]
      }
    ]
  };

}
