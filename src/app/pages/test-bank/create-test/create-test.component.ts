import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Rules, Exam, Student } from 'src/app/core/models';
import { Course } from 'src/app/core/models/course';
import { AuthenticationServiceService } from 'src/app/services/authentication-service.service';
import { ExamsService } from 'src/app/services/exams.service';
import { TestBankService } from 'src/app/services/test-bank.service';
import { AddStudentsToExamComponent } from '../../exam/create-exam/add-students-to-exam/add-students-to-exam.component';
import { UploadQuestionsComponent } from '../../exam/create-exam/upload-questions/upload-questions.component';
import { TestBankComponent } from '../test-bank.component';

@Component({
  selector: 'app-create-test',
  templateUrl: './create-test.component.html',
  styleUrls: ['./create-test.component.scss']
})
export class CreateTestComponent implements OnInit {

  public lang: any[] = [
    { value: 'en', viewValue: 'انجليزي' },
    { value: 'ar', viewValue: 'عربي' },
  ];
  public numberOfPages: any[] = [
    { value: '1', viewValue: '1' },
    { value: '2', viewValue: '2' },
  ];
  examInfoFormGroup: FormGroup;
  rulesFormGroup: FormGroup;
  duration: string;
  public currentExam: Exam;
  public years = [];
  public durationVisable = false;
  public color = 'primary';
  MODE = 'test_bank';
  public cousreNumbers: Set<any> = new Set();
  public cousreName: Set<any> = new Set();
  public courses: Course[];
  displayedColumns: string[] = ['studentName', 'studentNumber', 'level', 'email', 'password', 'delete'];
  public selectedStudents: Student = null;
  dataSource: any;
  tableIsVisable = false;

  constructor(private _formBuilder: FormBuilder,
              public dialog: MatDialog,
              private authServices: AuthenticationServiceService,
              private examsServices: ExamsService,
              private testBankServices: TestBankService, private _snackBar: MatSnackBar ) {

  }

  ngOnInit(): void {
    this.examInfoFormGroup = this._formBuilder.group({
      examType: new FormControl(6, [Validators.required]),
      courseName: new FormControl('', [Validators.required]),
      // questionsCount: new FormControl('', [Validators.required]),
      // semester: new FormControl('', [Validators.required]),
      courseNumber: new FormControl('', [Validators.required]),
      // year: new FormControl('', [Validators.required]),
      // fullMark: new FormControl('', [Validators.required]),
      // examDate: new FormControl('', [Validators.required]),
      // start: new FormControl('', []),
      // end: new FormControl('', []),
      // duration: new FormControl('', [Validators.required]),
      // maxGreade: new FormControl('', [Validators.required]),
      // markForEachQ: new FormControl('',[Validators.required])

    });
    this.rulesFormGroup = this._formBuilder.group({
      returnBack: new FormControl(false, [Validators.required]),
      isRandomSequence: new FormControl(false, [Validators.required]),
      numberOfPage: new FormControl('', [Validators.required]),
      examLanguage: new FormControl('', [Validators.required]),
      resultVisable: new FormControl(false, [Validators.required]),
      incorrectAnswersIsVisable: new FormControl(false, [Validators.required]),
      isRequiredQusetions: new FormControl(false, [Validators.required]),

    });

    // this.examInfoFormGroup.get('examType').disable();
    // this.examInfoFormGroup.get('duration').disable();
    // this.examInfoFormGroup.get('end').valueChanges.subscribe(ch => {
    //   const date = new Date().getHours().toLocaleString().toString() + ':' + new Date().getMinutes().toLocaleString().toString();
    //   const start = this.examInfoFormGroup.get('start').value;
    //   const end = this.examInfoFormGroup.get('end').value;
    //   const hour = (Number.parseInt(start.substr(0, start.lastIndexOf(':'))) - Number.parseInt(end.substr(0, end.lastIndexOf(':'))));
    //   const min = (Number.parseInt(start.substr(start.lastIndexOf(':') + 1)) - Number.parseInt(end.substr(end.lastIndexOf(':') + 1)));
    //   const checkPosistiveHour = hour < 0 ? (hour * -1) : hour;
    //   const checkPosistiveMin = min < 0 ? (min * -1) : min;
    //   this.duration = (checkPosistiveHour + ':' + checkPosistiveMin);
    //   this.examInfoFormGroup.controls.duration.setValue(this.duration);
    //   this.durationVisable = true;
    // });
    this.loadCourses();
    this.yearsRange();
  }

  private loadCourses() {
    this.examsServices.getCourses()
      .subscribe(arg => {
        console.log(arg);
        this.courses = arg.courses;
        this.courses.forEach(course => {
          this.cousreNumbers.add(course.courseNumber);
          this.cousreName.add(course.courseName);
        });
      });
  }

  save(): void {
    let exam = new Exam();
    let students = new Student();
    // this.examInfoFormGroup.controls.duration.setValue(this.duration);
    this.examInfoFormGroup.controls.examType.setValue('Test');
    exam = this.examInfoFormGroup.value;
    // exam.duration = this.duration;
    exam.examType = 6;
    students = this.selectedStudents;
    this.testBankServices.createTest(exam, students)
      .subscribe(arg => {
        console.log(arg);
        this.currentExam = arg.exam;
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
        }
      });
  }

  yearsRange(): any {
    let i = 0;
    for (let index = new Date().getFullYear(); index > 2000 ; index--) {
      this.years[i++] = index;
    }
  }

  uploadFromFile(): any {
    const dialogRef = this.dialog.open(UploadQuestionsComponent, {
      width: '1000px',
      height: '700px',
      data: { MODE: this.MODE, currentExam:  this.currentExam, }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  addStudents(): void {
    const dialogRef = this.dialog.open(AddStudentsToExamComponent, {
      width: '400px',
      height: '500px',
      data: { selectedStudents: this.selectedStudents }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.selectedStudents = new Student();
        this.selectedStudents = result;
        this.dataSource = new MatTableDataSource<Element>(result);
        this.tableIsVisable = true;
      }
    });
  }

  deleteStudent(student: Student): void {
    this.dataSource.data = this.dataSource.data
      .filter(i => i !== student)
      .map((i, idx) => (i.position = (idx + 1), i));
    this.dataSource._updateChangeSubscription();
    this.selectedStudents = this.dataSource.data;
  }
  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
