import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateExamComponent } from './exam/create-exam/create-exam.component';
import { ViewExamComponent } from './exam/view-exam/view-exam.component';
import { PreviewExamStatsComponent } from './exam/preview-exam-stats/preview-exam-stats.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { TestBankComponent } from './test-bank/test-bank.component';
import { GradesComponent } from './grades/grades.component';
import { StudentGradeViewComponent } from './grades/student-grade-view/student-grade-view.component';
import { DataComponent } from './data/data.component';
import { CreateTestComponent } from './test-bank/create-test/create-test.component';

const routes: Routes = [{
  path: 'dashboard',
  component: DashboardComponent,
  children: [
    {
      path: 'create-exam',
      component: CreateExamComponent
    },
    {
      path: 'view-exam',
      component: ViewExamComponent
    },
    {
      path: 'add-student',
      component: AddStudentComponent
    },
    {
      path: 'test-bank',
      component: TestBankComponent,
    },
    {
      path: 'grades',
      component: GradesComponent
    },
    {
      path: 'students/:id',
      component: StudentGradeViewComponent
    },
    {
      path: 'data',
      component: DataComponent
    },
    {
      path: 'create-test',
      component: CreateTestComponent
    },
  ],
},
  {
    path: 'preview-exam-stats/:examID',
    component: PreviewExamStatsComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
