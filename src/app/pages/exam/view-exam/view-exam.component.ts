import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Exam, Student } from 'src/app/core/models';
import { ExamsService } from 'src/app/services/exams.service';
import { StudentsListComponent } from '../students-list/students-list.component';
import {examTypes} from '../../../core/types';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { MessageDialogComponent } from 'src/app/shared/message-dialog/message-dialog.component';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-view-exam',
  templateUrl: './view-exam.component.html',
  styleUrls: ['./view-exam.component.scss']
})

export class ViewExamComponent implements OnInit,AfterViewInit {
  displayedColumns: string[] = [
    'examType', 'duration', 'questionsCount',
    'fullMark', 'courseName',
    'scale', 'created', 'courseNumber', 'opentExamDetails', 'students',
    'view-exam'
  ];
  public dataSource;
  public term: any;
  public cousreNumbers: Set<any> = new Set();
  public examTypes: any[] = [
    { value: examTypes.FIRST , viewValue: 'أول' },
    { value: examTypes.SECOND , viewValue: 'ثاني' },
    { value: examTypes.FINAL , viewValue: 'نهائي' },
    { value: examTypes.OTHER , viewValue: 'أخرى' },
  ];
  public exams: Exam[];
  public student: Student[];
  source = timer(0, 20);
  subscription: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private examsService: ExamsService, public dialog: MatDialog, private _snackBar: MatSnackBar,
    private route: Router
    ) {
      this.dataSource = new MatTableDataSource(this.exams);
    }

  ngOnInit(): void {
    this.loadExams();
  }

  private loadExams() {
    this.examsService.getExams()
      .subscribe(arg => {
        this.exams = arg.exams;
        this.dataSource.data = this.exams;
        this.exams.forEach(exam => {
          this.cousreNumbers.add(exam.courseNumber);
        });
      });
  }

  courseNumberFilter(e): void {
    const temStudents = this.exams;
    this.dataSource.data = this.exams.filter(exam => exam.courseNumber === e.value);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  kindOfExamFilter(e): void {
    const temStudents = this.exams;
    this.dataSource.data = this.exams.filter(exam => exam.examType === e.value);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getStudents(exam: Exam): void {
    const dialogRef = this.dialog.open(StudentsListComponent, {
      width: '1700px',
      height: '510px',
      data: { mode: 1, examObject: exam }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.subscription = this.source.subscribe(val => {
          this.loadExams();
          this.subscription.unsubscribe();
        });
      }
    });
  }


  delete(_id: string): void {

  }

  confirmAssement(courseNumber: any, examType: any) : any {

    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '230px',
      height: '210px',
      data: { title:  'رسالة تأكيد!', bodyMessage: 'هل أنت متأكد من الاعتماد' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        let confirmAssement = examType === examTypes.FIRST? {isFirstConfirmed: true}: {isSecondConfirmed: true}
        this.examsService.confirmAssement(confirmAssement,examType,courseNumber)
          .subscribe(arg => {
            if (arg.Confirmed) {
              this.openSnackBar('تم الأعتماد', 'suc-snackbar');
            } else {
              this.openSnackBar('حدث خطأ', 'err-snackbar');
            }
          });
      }
    });

  }

  previewExam(examID){
    const url = '/MainView/pages/preview-exam-stats/' + examID;
    this.route.navigate([url]);
    // const host: string =  location.origin;
    // const url: string = host + '/#/' + String(this.route.createUrlTree(['/MainView/pages/preview-exam-stats/'+ examID]));
    // window.open(url, '_blank');
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 4000,
      panelClass: [syle]
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
