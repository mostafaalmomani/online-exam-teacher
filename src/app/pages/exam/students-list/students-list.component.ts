import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Student } from 'src/app/core/models';
import { Mark } from 'src/app/core/models/mark';
import { ExamsService } from 'src/app/services/exams.service';
import { ViewExamComponent } from '../view-exam/view-exam.component';
import { examTypes } from 'src/app/core/types';
import  html2canvas from 'html2canvas';
import { TableUtil } from "../../../shared/tableUtil";
import * as XLSX from "xlsx";
import { environment } from 'src/environments/environment';
import { MatPaginator } from '@angular/material/paginator';
@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})
export class StudentsListComponent implements OnInit, AfterViewInit {

  @ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;

  public students: Student[] = [];
  displayedColumns: string[] = ['studentName', 'studentNumber', 'level', 'email', 'mark', 'answer'];
  dataSource: MatTableDataSource<unknown>;
  head = [['Name','Student number','Email','Level','Mark','Exam','Course']];
  pdfData = [];
  title = '';
  public examMark: any[] = [];
  public examDesc: any[] = [];
  public examAbswers: any[] = [];
  public examAbswersRes: any[] = [];
  marks: Mark[] = [];
  markEntity: Mark[] = [];
  dataImage: any;
  columnNames;
  exportData;
  public examType: any[] = [
    { value: examTypes.FIRST, viewValue: 'first' },
    { value: examTypes.SECOND, viewValue: 'second' },
    { value: examTypes.FINAL, viewValue: 'final' },
    { value: examTypes.OTHER, viewValue: 'other' },
  ];
  examTypValue;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private examsService: ExamsService,
              public dialogRef: MatDialogRef<ViewExamComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private _snackBar: MatSnackBar) {
                this.dataSource = new MatTableDataSource([]);
               }

  ngOnInit(): void {
    this.examTypValue = this.data.examObject.examType;
    this.loadStudentAnswers();
    this.loadStudentMarks();
  }

  private loadStudentMarks() {
    this.examsService.getStudents(this.data.examObject._id)
      .subscribe(arg => {
        this.students = arg.students;
        this.examsService.getStudentsMark(this.data.examObject._id, this.data.examObject.examType)
          .subscribe(arg2 => {
            this.marks = arg2.marks;
            this.title = this.data.examObject.courseName;
            let i = 0;
            this.students.forEach(student => {
              if (this.marks) {
                this.examMark[student._id] = this.findStudentMark(student._id, this.marks);
                this.examAbswers[student._id] = this.findFileName(student._id);
              }
            });
            this.filldataToExport(this.students, this.data.examObject, this.examMark);
          });
        this.dataSource.data = arg.students;
      });
  }

  findFileName(_id: string): any {
     let value = 0;
     this.examAbswersRes.forEach(element => {
       if (element.studentsId === _id) {
          value = element.path;
       }
     });
     return value;
  }

  private loadStudentAnswers() {
    this.examsService.getStudentsAnswers(this.data.examObject._id)
      .subscribe(arg => {
        console.log(arg)
        this.examAbswersRes = arg.answers;
      });
  }

  findStudentMark(_id: string, marks: Mark[]): number {
     let value = 0;
     marks.forEach(element => {
       if (element.studentsId === _id) {
          value = element.markPoint;
          value = Number.parseFloat(value.toFixed(2));
       }
     });
     return value;
    }

  saveMark(): void {
    this.students.forEach(student => {
      const mark = new Mark();
      mark.examId = this.data.examObject._id;
      mark.studentsId = student._id;
      mark.markPoint = this.examMark[student._id];
      mark.markDesc = this.examTypValue;
      this.markEntity.push(mark);
    })

    this.examsService.saveExamMarks(this.markEntity)
      .subscribe(arg => {
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  filldataToExport(students: Student[], examObject: any, marks: any[]) {
    students.forEach(element => {
      let dataObj =  {"اسم الطالب": element.studentName,"رقم الطالب": element.studentNumber,
      "البريد": element.email,"المرحلة": element.level, "العلامة" : marks[element._id],
      "نوع الأمتحان" : examObject.examType, "أسم المادة": examObject.courseName};

      // dataObj.push({"اسم الطالب" :element.studentName});
      // dataObj.push({"رقم الطالب" :element.studentNumber});
      // dataObj.push({"البريد" :element.email});
      // dataObj.push({"المرحلة" :element.level});
      // dataObj.push({"العلامة" :marks[element._id]});
      // dataObj.push({"نوع الأمتحان" :examObject.examType});
      // dataObj.push({"أسم المادة" :examObject.courseName});

      this.pdfData.push(dataObj);
      dataObj = null;
    });

    this.columnNames = Object.keys(this.pdfData[0]);
    this.exportData = this.pdfData

  }

  exportArray(): any {
    TableUtil.exportArrayToExcel(this.pdfData, this.title);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  capitalize(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
  }
  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }

  downloadMyFile(path: any,studentNumber: any): any {
    this.examsService.getStudentFileAnswers(path,studentNumber);
    // console.log(path);
    // const apiUrl = 'http://167.99.134.43:80/api/v1/';
    // // const apiUrl = 'http://localhost:11200/api/v1/';
    // const link = document.createElement('a');
    // // link.setAttribute('target', '_blank');
    // link.setAttribute('href', `${apiUrl}readImage/${path}`);
    // link.setAttribute('download', `answers.pdf`);
    // document.body.appendChild(link);
    // link.click();
    // link.remove();
  }

}


