import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { ExamsService } from '../../../services/exams.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeUrl  } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-preview-exam-stats',
  templateUrl: './preview-exam-stats.component.html',
  styleUrls: ['./preview-exam-stats.component.scss']
})
export class PreviewExamStatsComponent implements OnInit {
  pages: any[] = [];
  Questions: { num, label, answers: any, studentAnswer, _id: any, mark, image, total }[] = [];
  loaded:boolean = false;
  examID: string;
  questionObj:any = {};
  numberOfQusetion: number;
  title: string;
  constructor(private examService:ExamsService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.examID = params.examID;
      this.examService.getExamInfo(this.examID)
        .subscribe(examInfo => {
          this.title = examInfo.courseName;
          this.examService.getQusetion(this.examID)
          .subscribe(arg => {
            this.questionObj = arg;
            this.numberOfQusetion =  this.questionObj.length;
            this.getQuetions();
            this.renderPages(); 
            this.loaded = true;
          });
        });
    });
  }

  getQuetions(): any {
    Object.keys(this.questionObj).map((q, index) => {
      this.Questions.push({
        num: index + 1, studentAnswer: '', label: this.questionObj[q].questionText, total: this.questionObj[q].total,
        _id:  this.questionObj[q]._id, mark: this.questionObj[q].point, image: this.questionObj[q].imageQ,
        answers: Object.keys(this.questionObj[q].answers).map(a => this.questionObj[q].answers[a])
      });
      
    });
  }

  renderPages(): any {
    for (let i = 0; i < this.Questions.length; i++) {
      this.pages.push({
        Num: i + 1,
        Questions: this.Questions.slice(i, i + 1)
        // Questions: this.Questions.slice(this.numOfQperPage * i, (this.numOfQperPage * i) + this.numOfQperPage)
      });
    }
  }

}


@Pipe({
  name: 'calcpercentage'
})
export class CalcPercentagePipe implements PipeTransform {

  constructor() {
  }
  transform(value: any, ...args: any[]): any {
    if(typeof(value) == 'undefined') return 0;
    let num = (value / args[0]) * 100 / 100;
    return Math.round((num + Number.EPSILON) * 100) / 100;
  }
}
