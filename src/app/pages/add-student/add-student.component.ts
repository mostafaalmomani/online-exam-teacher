import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Student } from 'src/app/core/models';
import { AuthenticationServiceService } from 'src/app/services/authentication-service.service';
import { StudentServicesService } from 'src/app/services/student-services.service';
import { MessageDialogComponent } from 'src/app/shared/message-dialog/message-dialog.component';
import { StudentCardAddEditComponent } from './student-card-add-edit/student-card-add-edit.component';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit {
  displayedColumns: string[] = ['studentName', 'studentNumber', 'level', 'email', 'delete'];
  public dataSource;
  source = timer(0, 20);
  subscription: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private fb: FormBuilder, public dialog: MatDialog,
    private studentservice: StudentServicesService, private _snackBar: MatSnackBar) {
      this.dataSource = new MatTableDataSource([]);
     }

  ngOnInit(): void {
    this.loadStudents();

  }


  private loadStudents() {
    this.studentservice.getStudent()
      .subscribe(arg => {
        this.dataSource.data = arg.students;
      });
  }

  addStudent(): void {
    const dialogRef = this.dialog.open(StudentCardAddEditComponent, {
      width: '500px',
      height: '510px',
      data: { mode: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
          this.loadStudents();
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  edit(student: Student): void {
    const dialogRef = this.dialog.open(StudentCardAddEditComponent, {
      width: '500px',
      height: '510px',
      data: { mode: 1, studentObject: student }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.subscription = this.source.subscribe(val => {
          this.loadStudents();
          this.subscription.unsubscribe();
        });
      }
    });
  }

  delete(_id: string): void {
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '230px',
      height: '210px',
      data: { title:  'رسالة تأكيد!', bodyMessage: 'هل أنت متأكد من الحذف؟' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.studentservice.deleteStudent(_id)
          .subscribe(arg => {
            this.openSnackBar('تم الحذف', 'suc-snackbar');
            this.subscription = this.source.subscribe(val => {
              this.loadStudents();
              this.subscription.unsubscribe();
            });

          });

      }
    });
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

}
