import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StudentServicesService } from 'src/app/services/student-services.service';
import { AddStudentComponent } from '../add-student.component';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { Student } from 'src/app/core/models';
import { MatSelectionList } from '@angular/material/list';
import { timer, Subscriber, Observable } from 'rxjs';

@Component({
  selector: 'app-student-card-add-edit',
  templateUrl: './student-card-add-edit.component.html',
  styleUrls: ['./student-card-add-edit.component.scss']
})
export class StudentCardAddEditComponent implements OnInit {

  addStudentForm: FormGroup;
  levels = ['1', '2', '3', '4', '5', '6', 'أخرى'];
  hide = true;
  public originalStudents: Student[] = [];
  public students: Student[] = [];
  public term: any;
  public levelModel: any;
  allSelected = false;
  @ViewChild('Students', { static: false }) studentsRef: MatSelectionList;
  source = timer(0, 20);
  subscription: any;
  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar,
    private studentservice: StudentServicesService,
    public dialogRef: MatDialogRef<AddStudentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.loadAllStudents();
    this.createStudent();
  }

  private loadAllStudents() {
    this.studentservice.getAllStudents()
      .subscribe(arg => {
        this.students = arg.students;
        this.originalStudents = this.students;
      });
  }

  private createStudent() {
    this.addStudentForm = this.fb.group({
      studentName: ['', Validators.required],
      // studentNumber: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required],
      level: ['', Validators.required],
      phone: ['', Validators.required],
    });
    if (this.data.mode === 1) {
      this.addStudentForm.controls.studentName.setValue(this.data.studentObject.studentName);
      // this.addStudentForm.controls.studentNumber.setValue(this.data.studentObject.studentNumber);
      this.addStudentForm.controls.email.setValue(this.data.studentObject.email);
      this.addStudentForm.controls.password.setValue(this.data.studentObject.NonHashedPassword);
      this.addStudentForm.controls.level.setValue(this.data.studentObject.level);
      this.addStudentForm.controls.phone.setValue(this.data.studentObject.phone);
    }
  }

  // onSubmit(): void {
  //   let student = new Student();
  //   student = this.addStudentForm.value;
  //   if (this.data.mode === 0) {
  //     this.studentservice.addStudent(student)
  //       .subscribe(arg => {
  //         console.log(arg);
  //         if (arg.message.match('saved!')) {
  //           this.openSnackBar('تم الحفظ', 'suc-snackbar');
  //         }
  //       },
  //         (error) => {
  //           console.log(error);
  //           if (error) {
  //             this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
  //           }
  //         });
  //     }  else if (this.data.mode === 1) {
  //       student._id = this.data.studentObject._id;
  //       this.studentservice.editStudent(student)
  //       .subscribe(arg => {
  //         console.log(arg);
  //         if (arg.message.match('saved!')) {
  //           this.openSnackBar('تم الحفظ', 'suc-snackbar');
  //         }
  //       },
  //         (error) => {
  //           console.log(error);
  //           if (error) {
  //             this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
  //           }
  //         });
  //     }
  //   this.dialogRef.close(true);
  // }

  onNoClick(): void {
    this.dialogRef.close(false);
  }


  onSubmit(): void {
    const students = [];
    const selectedStudents = this.studentsRef.selectedOptions.selected;
    for (let index = 0; index < selectedStudents.length; index++) {
      const element = selectedStudents[index].value;
      students.push(element);
    }

    this.studentservice.setStudentsToCurrentTeacher(students)
      .subscribe(arg => {
        if (arg.message.match('saved!')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
          this.subscription = this.source.subscribe(val => {
            this.loadAllStudents();
            this.dialogRef.close(true);
            this.subscription.unsubscribe();
          });
        }
      },
        (error) => {
          console.log(error);
          if (error) {
            this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
          }
        })
    // this.dialogRef.close(students);
  }

  selectAll(): any {
    this.allSelected = !this.allSelected;
    if (this.allSelected) {
      this.studentsRef.selectAll();
    } else {
      this.studentsRef.deselectAll();
    }
  }

  filter(e): void {
    const temStudents = this.students;
    this.students = this.originalStudents.filter(student => student.level === e.value);
  }


  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
