import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Exam } from 'src/app/core/models';
import { Course } from 'src/app/core/models/course';
import { ExamsService } from 'src/app/services/exams.service';
import { MessageDialogComponent } from 'src/app/shared/message-dialog/message-dialog.component';
import { AddEditCourseComponent } from './add-edit-course/add-edit-course.component';
import { timer, Subscriber, Observable } from 'rxjs';

@Component({
  selector: 'app-grades',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.scss']
})
export class GradesComponent implements OnInit {

  displayedColumns: string[] = [
    'courseName', 'courseNumber',
    'created', 'grades', 'scales', 'confirm'
  ];
  public dataSource: Course[];
  public term: any;
  public cousreNumbers: Set<any> = new Set();
  public cousreName: Set<any> = new Set();
  public courses: Course[];
  source = timer(0, 20);
  subscription: any;
  coursesNum;
  constructor(private route: ActivatedRoute, private examsService: ExamsService,
    public dialog: MatDialog, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.loadCourses();
  }

  private loadCourses() {
    this.examsService.getCourses()
      .subscribe(arg => {
        this.courses = arg.courses;
        this.courses.forEach(course => {
          this.cousreNumbers.add(course.courseNumber);
          this.cousreName.add(course.courseName);
        });
      });
  }

  courseNumberFilter(e): void {
    this.coursesNum = e.value;
    const temStudents = this.courses;
    this.dataSource = this.courses.filter(exam => exam.courseNumber === e.value);

  }

  kindOfExamFilter(e): void {
    console.log(e.value);
    const temStudents = this.courses;
    this.dataSource = this.courses.filter(course => course.courseName === e.value);
  }

  addCourse(): any {
    const dialogRef = this.dialog.open(AddEditCourseComponent, {
      width: '500px',
      height: '560px',
      data: { mode: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.subscription = this.source.subscribe(val => {
          this.loadCourses();
          this.subscription.unsubscribe();
        });
      }
    });
  }

  addScales(courseObject: string): any {
    const dialogRef = this.dialog.open(AddEditCourseComponent, {
      width: '500px',
      height: '560px',
      data: { mode: 0, courseObject: courseObject }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        window.location.reload();
        // this.subscription = this.source.subscribe(val => {
        //   this.loadCourses();
        //   this.subscription.unsubscribe();
        // });
      }
    });
  }

  confirmAssement(courseNumber: string): any {

    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '230px',
      height: '210px',
      data: { title: 'رسالة تأكيد!', bodyMessage: 'هل أنت متأكد من الأعتماد' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.examsService.confirmFinalMark(courseNumber)
          .subscribe(arg => {
            if (arg.Confirmed) {
              this.openSnackBar('تم الأعتماد', 'suc-snackbar');
            } else {
              this.openSnackBar('حدث خطأ', 'err-snackbar');
            }
          }, (err) => {
            console.log(err);
            this.openSnackBar('حدث خطأ', 'err-snackbar');
          });
      }
    });
  }

  getStudent(courseNumber: string): any {
    // this.route.navigate(['/products', courseNumber]);
    // this.examsService.getCoursesStudents(courseNumber)
    //   .subscribe(arg => {
    //     console.log(arg.students);
    //   });
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
