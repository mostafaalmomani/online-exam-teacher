import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';
import { MatSliderChange } from '@angular/material/slider';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Student } from 'src/app/core/models';
import { Course } from 'src/app/core/models/course';
import { ExamsService } from 'src/app/services/exams.service';
import { StudentServicesService } from 'src/app/services/student-services.service';
import { AddStudentComponent } from '../../add-student/add-student.component';

@Component({
  selector: 'app-add-edit-course',
  templateUrl: './add-edit-course.component.html',
  styleUrls: ['./add-edit-course.component.scss']
})
export class AddEditCourseComponent implements OnInit {

  levels = ['أ+', 'أ', 'أ-', 'ب+ ', 'ب', 'ب- ', 'ج+', 'ج', 'ج-', 'د+', 'د', 'هـ'];
  panelOpenState = false;
  addCourseForm: FormGroup;
  min: any;
  max: any;
  scales: any[] = [];
  coursesScales: any[] = [];
  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar,
              private examervice: ExamsService,
              public dialogRef: MatDialogRef<AddStudentComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.addCourseForm = this.fb.group({
      courseName: ['', Validators.required],
      // courseNumber: ['', Validators.required],
      scale: ['', Validators.required],
    });
    this.coursesScales = this.data.courseObject.scales;
    this.addCourseForm.controls.courseName.setValue(this.data.courseObject.courseName);
    if (this.data.mode === 1) {
      this.addCourseForm.controls.courseName.setValue(this.data.courseObject.courseName);
      // this.addCourseForm.controls.courseNumber.setValue(this.data.courseObject.courseNumber);
      this.addCourseForm.controls.scale.setValue(this.data.courseObject.scale);
    }
    //console.log(this.data);
  }

  onSubmit(): void {
    const course = new Course();
    course.courseName = this.addCourseForm.controls.courseName.value;
    // course.courseNumber = this.addCourseForm.controls.courseNumber.value;
    course.scales = this.scales;
    course._id = this.data.courseObject._id;
    if (this.data.mode === 0) {
      this.examervice.editCourse(course)
        .subscribe(arg => {
          console.log(arg);
          if (arg.message.match('saved!')) {
            this.openSnackBar('تم الحفظ', 'suc-snackbar');
          }
        },
          (error) => {
            console.log(error);
            if (error) {
              this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
            }
          });
    } else if (this.data.mode === 1) {
      // student._id = this.data.studentObject._id;
      // this.studentservice.editStudent(student)
      //   .subscribe(arg => {
      //     console.log(arg);
      //     if (arg.message.match('saved!')) {
      //       this.openSnackBar('تم الحفظ', 'suc-snackbar');
      //     }
      //   },
      //     (error) => {
      //       console.log(error);
      //       if (error) {
      //         this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
      //       }
      //     });
    }
    this.dialogRef.close(true);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }

  saveMax(event): any {
    console.log(event.target.value);
    this.max = event.target.value;
  }

  saveMin(event): any {
    console.log(event.target.value);
    this.min = event.target.value;
  }

  saveScale(): any {
    this.scales.push({ degree: this.addCourseForm.controls.scale.value, min: this.min, max: this.max });
    console.log(this.scales);
  }

  formatLabel(value: number): any {
    if (value >= 100) {
      return 100;
    }
    return value;
  }
}
