import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

    apiUrl = 'http://localhost:80/api/v1/'
   // apiUrl = 'https://api.exams-tabeeb.org/api/v1/'
  constructor() { }
}
