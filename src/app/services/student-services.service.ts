import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Student } from '../core/models';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class StudentServicesService extends BaseService{

  constructor(private http: HttpClient) {
    super();
  }


  addStudent(student: Student): any {
    return this.http.post<{message: any}>(`${this.apiUrl}register/student`, {student});
  }

  editStudent(student: Student): any {
    return this.http.post<{message: any}>(`${this.apiUrl}updatestudents`, {student});
  }

  getStudent(): any {
    return this.http.get<{students: Student}>(`${this.apiUrl}getstudents`);
  }

  getAllStudents(): any {
    return this.http.get<{students: Student}>(`${this.apiUrl}getAllStudents`);
  }

  setStudentsToCurrentTeacher(students: Student[]): any {
    return this.http.post<{students: Student}>(`${this.apiUrl}setStudents`,{students});
  }

  deleteStudent(id: string): any {
    return this.http.delete<{message: any}>(`${this.apiUrl}deletestudents/${id}`);
  }
}
