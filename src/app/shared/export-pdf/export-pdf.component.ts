import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import {font} from '../../../assets/js/DroidKufi-Regular-normal.js';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-export-pdf',
  templateUrl: './export-pdf.component.html',
  styleUrls: ['./export-pdf.component.scss']
})
export class ExportPdfComponent implements OnInit {
  @Input() head;
  @Input() data = [];
  @Input() title;
  @Input() imageData;

  displayedColumns: string[] = [];
  dataSource = [];
  columnNames;

  @ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;
  constructor() { }

  ngOnInit(): void {
  }

  createPdf() {
    let doc = new jsPDF('p','pt', 'a4');
    //console.log(font);

    var callAddFont = function () {
      this.addFileToVFS('../../../assets/DroidKufi-Regular-normal.ttf', font);
      this.addFont('DroidKufi-Regular-normal.ttf', 'DroidKufi-Regular', 'normal');
    };
    jsPDF.API.events.push(['addFonts', callAddFont]);
    // var imgWidth = 208;
    // var imgHeight = this.imageData.height * imgWidth / this.imageData.width;
    // const contentDataURL = this.imageData.toDataURL('image/png')
    // doc.addFileToVFS('DroidKufi-Regular.ttf', font);
    // doc.addFont('DroidKufi-Regular.ttf', 'DroidKufi-Regular', 'normal');
    doc.setFont("DroidKufi-Regular",'normal');
    // var position = 0;
    // doc.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
    doc.setFontSize(18);
    doc.viewerPreferences({ Direction: 'R2L' });

    doc.setLanguage("ar-JO");
    doc.setTextColor(100);
    (doc as any).autoTable({
      head: this.head,
      body: this.data,
    })
    doc.save(this.title+'.pdf');
  }

  capitalize(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
  }

  downloadImage(): void{
    // html2canvas(this.screen.nativeElement).then(canvas => {
       this.canvas.nativeElement.src = this.imageData.toDataURL();
      // this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      // this.downloadLink.nativeElement.download = 'marble-diagram.png';
      // this.downloadLink.nativeElement.click();

      const doc = new jsPDF('p', 'pt', 'a4');
      const imgWidth = 600;
      const imgHeight = this.imageData.height * imgWidth / this.imageData.width;
      const contentDataURL = this.imageData.toDataURL('image/png');
      const position = 0;
      doc.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      doc.save(this.title + '.pdf');

    // });
  }
}




