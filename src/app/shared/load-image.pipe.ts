import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl  } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'loadImage'
})
export class LoadImagePipe implements PipeTransform {

  imageToShow: SafeUrl | null = null;
  apiUrl = 'http://localhost:80/api/v1/'
   //apiUrl = 'https://api.exams-tabeeb.org/api/v1/'
  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
  }

  transform(value: any, ...args: any[]): any {
    const headers = new Headers({'Content-Type': 'image/*'});
    return `${this.apiUrl.replace('v1','v2')}readImage/${value}`;
  }
}
